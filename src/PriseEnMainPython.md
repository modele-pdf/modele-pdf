<h1 style="text-align:center;">Chapitre 1 : initiation à Python </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "python.png" alt='image'>
</div> 

<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "sammet.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Jean_E._Sammet' target="_blank">Jean E. Sammet</a>
(23 mars 1928 - 20 mai 2017) est une informaticienne américaine, une des pionnières de l'informatique et des langages de programmation. Elle développe en 1962 le langage de programmation FORMAC (Formula Manipulation Compiler), une extension de FORTRAN conçue pour les manipulations symboliques de formules mathématiques. Elle est également l’une des développeuses du langage de programmation COBOL (Common Business Oriented Language).
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div>
<br>

<p> Première initiation à Python et au langage de programmation avec un peu d'histoire. On présente différents environnements de développement.
Pour l’enseignement de spécialité NSI, nous avons choisi de programmer avec le logiciel <a href='https://www.edupyter.net/' target="_blank">EduPyter</a> et avec l’application <a href='https://capytale2.ac-paris.fr/web/c-auth/list' target="_blank">Capytale</a>. Afin de prendre en main cette dernière, on propose deux notebooks :
<ul>
<li><code>NSI Première Partie 1 Chapitre 1 Prise en main Notebook Jupyter 1/2</code> (les bases) ;</li>
<li><code>NSI Première Partie 1 Chapitre 1 Prise en main Notebook Jupyter 2/2</code> (pour aller plus loin).</li>
</ul>  </p>

<br>
<br>

<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','PriseEnMainPython.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Booleens_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;display:none;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Accès via Capytale</h3>

Dans la zone Rechercher, taper les phrases ci-dessous :

    - NSI Première Partie 1 Chapitre 1 Prise en main Notebook Jupyter 1/2

    - NSI Première Partie 1 Chapitre 1 Prise en main Notebook Jupyter 2/2


<h3>Téléchargement</h3>
<a href="https://nuage03.apps.education.fr/index.php/s/QckfMj3R6mf4ine" target="_blank">Téléchargement des ressources</a>
<h1 style="text-align:center;">Partie 5 : réseaux</h1>

<br>
<div style="text-align: center;"> 
<img src = "Reseau.png" alt='image'>
</div>
<br>

Les réseaux informatiques permettent de transmettre des informations entre différentes machines (ordinateurs, serveurs, imprimantes, téléphones, voitures, etc.) à travers divers types de connexion (filaire, sans-fil, optioque).
<br><br>
Ces transmissions sont réalisées à partir de règles de communication appelées <span style='font-style:italic;'>protocoles</span>.
<br><br>
Chaque machine est repérée sur le réseau à partir d'une adresse unique appelée <span style='font-style:italic;'>adresse IP</span>.

<br><hr><br>
Cette partie, entièrement consacrée aux réseaux, abordera les points suivants :

<br>

1. Réseaux :
    - histoire et évolution ;
    - protocoles ;
    - éléments physiques ;
    - modèle TCP/IP ;
    - couches réseaux ;
    - adresse IP et notation CIDR.

<br>

2. Bit alterné :
    - principe général du protocole du bit alterné ;
    - minuteur, acquittement et duplicata.
<h1 style="text-align:center;">Partie 1 : programmation Python</h1>
<br>
<div style="text-align: center;"> 
<img src = "Programmation_python.png" alt='image programmation python' style='width:40%; height:auto;'>
</div>
<br>

Les langages de programmation Turing-complets sont caractérisés par un corpus de _constructions élémentaires_. Sans introduire cette terminologie, il s’agit de montrer qu’il existe de nombreux langages de programmation, différents par leur style (impératif, fonctionnel, objet, logique, événementiel, etc.), ainsi que des langages formalisés de description ou de requêtes qui ne sont pas des langages de programmation.

L’importance de la spécification, de la documentation et des tests est à présenter, ainsi que l’intérêt de la modularisation qui permet la réutilisation de programmes et la mise à disposition de bibliothèques. Pour les programmes simples écrits par les élèves, on peut se contenter d’une spécification rapide mais précise.

<br><hr><br>
Cette partie, entièrement consacrée à la programmation Python, abordera les points suivants :

<br>

1. Constructions élémentaires :
    - variable et affectation ;
    - séquence d'instructions ;
    - fonction ;
    - structures d'embranchement ;
    - structures de boucle (itératives et conditionnelles).


# Summary

[Introduction](introduction.md)
[Prise en main](prise.md)


- [Partie 1 : programmation Python](partie1.md)
    - [Initiation à Python](PriseEnMainPython.md)
    - [Variables et types](Variables_types.md)
    - [Les fonctions](Fonctions.md)

- [Partie 2 : représentation des données](partie2.md) 
    - [Booléens](Booleens.md)
    - [Représentation des entiers naturels](codage_entiers.md)

- [Partie 3 : réseaux](partie5.md)
    - [Réseaux](Reseaux.md)
    - [Bit alterné](BitAlterne.md)

   
[Bibliographie et sitographie](partie7.md)

[Remerciements et crédits](credits.md)
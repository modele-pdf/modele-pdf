<h1 style="text-align:center;">Partie 2 : représentation des données</h1>
<br>
<div style="text-align: center;"> 
<img src = "Ecran_binaire.png" alt='écran code binaire' style='width:40%; height:auto;'>
</div>
<br>

Toute machine informatique manipule une représentation des données dont l’unité minimale est le bit 0/1, ce qui permet d’unifier logique et calcul. Les données de base sont représentées selon un codage dépendant de leur nature : entiers, flottants, caractères et chaînes de caractères. Le codage conditionne la taille des différentes valeurs en mémoire.

<br><hr><br>
Cette partie, entièrement consacrée à la représentation des données sous forme binaire, abordera les points suivants :

<br>

1. Booléens :
    - valeurs booléennes ;
    - opérateurs booléens : and, or et not ;
    - expressions booléennes ;
    - manipulation de tables de vérité.

<br>

2. Codage des entiers naturels :
    - représentation binaire ;
    - représentation hexadécimale ;
    - passage entre les deux représentations.

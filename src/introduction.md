
# Préambule

<br>
<div style="text-align: center;"> 
<img src = "intro.png" alt='image1'>
</div> 
<br>


Le but de ce site est de proposer un cours complet de Première NSI dans le respect des programmes du <a href='https://eduscol.education.fr/document/30007/download' target="_blank">Bulletin Officiel</a>. Il regroupe de nombreuses ressources (cours, TP, exercices et projets), fruits d'une collaboration entre trois enseignants de la spécialité NSI : Mathilde Boehm (académie de Lyon), Charles Poulmaire (académie de Versailles) et Pascal Remy (académie de Versailles). 

Toutes ces ressources, ainsi que les intentions et stratégies pédagogiques, sont partagées gratuitement sous licence <a href='https://creativecommons.org/licenses/by-sa/4.0/' target="_blank">CC BY-SA 4.0</a> afin de fournir des <span style='font-style:italic;'>Ressources Educatives Libres</span> (en abrégé, <span style='font-style:italic;'>REL</span>) comme <span style='font-style:italic;'>communs numériques</span>. Le but est de permettre à toutes et tous, de pouvoir accéder à des contenus pédagogiques, de les adapter en fonction des besoins spécifiques des élèves et de les ajuster pour répondre aux différents styles d'apprentissage et aux niveaux de compétence de vos élèves.

<br>
<hr>
<br>
Les auteurs encouragent vivement le partage, la modification et la réutilisation des contenus afin que tout le monde puisse collaborer pour améliorer les ressources existantes, entraînant ainsi une synergie collective et une richesse de connaissances partagées. Les contenus peuvent être mis à jour pour refléter les avancées et les changements dans les domaines de connaissances afin de toujours garder des contenus pertinents et actuels.

<br>

Les auteurs soutiennent également une approche ouverte, collaborative et inclusive de l'Education, qui favorise la diffusion du savoir, l'innovation et l'équité dans l'accès à l'Education. L’exploitation gratuite et sans restrictions d'utilisation des REL sont rendues possible par les cinq R, piliers des REL :

- RETENIR : télécharger, dupliquer, conserver, le contenu autant de fois ou aussi longtemps que vous le voulez ;
- RÉUTILISER : utiliser le contenu de diverses manières selon vos propres fins ;
- RÉVISER : adapter, modifier ou traduire la ressource ;
- REMIXER : combiner la ressource (ou ses modifications) à une autre ;
- REDISTRIBUER : partager la ressource (ou ses modifications) avec d’autres.


Enfin, les auteurs soutiennent toutes les actions ayant pour objectif la mixité, notamment dans les Sciences et plus spécifiquement ici dans l'Informatique. Ainsi, ils ont vivement souhaité mettre en avant les rôles des femmes dans l'Informatique en choisissant de débuter chacun des trente-cinq chapitres de ce cours par un portrait de femme informaticienne.





<h1 style="text-align:center;">Chapitre 1 : les booléens </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<div style="text-align: center;"> 
<img src = "booleen1.png">
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "monica.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Monica_S._Lam' target="_blank">Monica Sin-Ling Lam</a>
Monica Sin-Ling Lam est une informaticienne américaine et chercheuse en compilation de programmes et en systèmes de langages de programmation. Elle est professeure au département d'informatique de l'université Stanford. 
Elle contribue à la recherche sur un large éventail de sujets relatifs aux systèmes informatiques, notamment les compilateurs, l'analyse de programmes, les systèmes d'exploitation, la sécurité, l'architecture matérielle et l'informatique haute performance.  
</p>
<br>
</div>

<br>
<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Entre 1844 et 1854, un mathématicien britannique, George Boole, crée une algèbre binaire, dite <span style='font-style:italic;'>booléenne</span>, n’acceptant que deux valeurs numériques : 0 et 1.
<br><br>
Grâce à Claude Shannon en 1938, cette algèbre aura de nombreuses applications en téléphonie et en informatique, notamment pour la représentation des données dont l’unité minimale est le bit 0/1.
<br><br>
Le but de ce chapitre est de présenter la notion de booléens et de manipuler quelques expressions booléennes simples.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un point de cours pour présenter les variables booléennes, les opérateurs booléens et les expressions booléennes avec, notamment, la manipulation de tables de vérité ;</li>
<li>des exercices d'entraînement pour manipuler tous les concepts vus dans le cours.</li>
</ol>
<br><br>

<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Booleens_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Booleens_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>

